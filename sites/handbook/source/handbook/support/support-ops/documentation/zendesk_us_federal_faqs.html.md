---
layout: handbook-page-toc
title: 'Zendesk US Federal FAQs'
category: 'FAQs'
description: 'Zendesk US Federal frequently asked questions'
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

This is a compilation of frequently asked questions (FAQs) for Zendesk Global.

### Can customers of this instance use the Global instance?

Yes they can! Because of the limited hours Zendesk US Federal is available, we
enable all US Federal customers to utilize Zendesk Global, with the
understanding that Zendesk Global is not restricted to US citizens only.
