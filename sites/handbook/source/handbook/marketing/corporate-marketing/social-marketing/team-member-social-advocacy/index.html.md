---
layout: handbook-page-toc
title: Team Member Social Media Advocacy
description: Strategies and details to enable team members to share GitLab-related news on personal social media channels
twitter_image: /images/opengraph/handbook/social-marketing/social-handbook-top.png
twitter_image_alt: GitLab's Social Media Handbook branded image
twitter_site: gitlab
twitter_creator: gitlab
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## ⚠️ `THIS HANDBOOK PAGE IS UNDER CONSTRUCTION AND IS NOT CONSIDERED TO BE LIVE ADVICE AT THIS TIME` Please reach out to #social_media_action Slack channel with any questions

## Team Member Social Media Advocacy and Enablement

[According to Sprout Social](https://sproutsocial.com/insights/what-is-employee-advocacy/), team member advocacy is the promotion of an organization by its staff members. People trust recommendations and content from people they know. They trust these people a lot more than they trust marketing messages from companies on organic brand social channels or paid social media advertising. We know this when a team member can post the same content the brand channel publishes, but because the message was more personal, more human, the team member gains significant engagement over the brand channel.

### Social Advocacy Strategy

[TOP LEVEL STRATEGY OUTLINE; HOW WE'LL MEASURE, ETC]

#### Goals of social media advocacy 

- Improve the organic reach of GitLab messaging
- Increase traffic to our site from social media, particularly to blogs and campaign content
- Reduce brand and team member risk by providing curated content to publish

#### Benefits of social advocacy to all team members

- Curated and prewritten drafts for social posts allow for easy publishing with minimal edits required
- Posts can be scheduled, like a social media manager would for the brand, allowing the work to be semi-automated
- Easy way to include incentives like swag, bonuses, or other add-ons
- Career growth: sharing industry content on social media can help make team members be looked at as thought leaders in their spaces

### Bambu, our social advocacy tool

[INSERT SOME SPECIFICS ABOUT BAMBU, ACCESS THROUGH OKTA, ETC]

#### Team Member Roles in Bambu, the advocacy program

##### Admins

The GitLab Social Team are the administrators of the social advocacy program. Admins have all access to our tool, Bambu, as needed to operate the program. Admins may act as curators from time to time as well.

If you have questions or would like to learn more, consider sending a message in the #social-advocacy Slack channel.

| Name | Role |
| ------ | ------ |
| Kristen Sundberg | Curator and Program Admin |
| Wil Spillane | Technology Owner/Admin |

##### Curators (or Contributors)

##### If you're a curator or interested in becoming a curator, [head to our curator-specific handbook page here]tbd)

Curators are selected intentionally to drive our advocacy content strategy and specific team members are asked to take on the role of a curator as a part of their everyday jobs at GitLab. We will have curators representing all areas of GitLab the brand and the product in order to curate a list of releated content worth sharing on social media. 

Curators have all Reader access as well as the ability to curate stories and submit them to a Manager or Admin for approval. 

Join the #social-advocacy-curators Slack channel to stay in touch with the curator program and the latest news. This channel is intended for team members who are identified as `content curators` only.

##### General Team Members (or Readers)

All team members can access the Stories feed, share to their social networks and leverage the Suggestions feature to submit links to a Manager or Admin for complete Story curation.

Access to Bambu is provided via an Okta tile - please log in to Okta and find the Bambu logo tile to Log on.

Join the #social-advocacy Slack channel to stay in touch with the program and the latest news.

### Reporting and Metrics 

[Bambu provides a report center to outline all of the possibilities here](https://bambu.zendesk.com/hc/en-us/articles/360020038351-Bambu-Report-Guide).
