---
layout: handbook-page-toc
title: "SOX Certification"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## 1. Sales Representation Letter


- **[Introduction](/handbook/sales/commissions/#introduction)**

- **[Sales representation certificate](/handbook/sales/commissions/#sales-representation-letter-certifies)**

- **[Sales representation certification timelines](/handbook/sales/commissions/#sales-rep-certification-process-timeline)**

- **Sales certification process**
                                                                                 
     <ins>Steps followed by Internal Audit for Sales Representation Certification Process:</ins> 

     1. On the first day of the subsequent month after the quarter-end - <br>
          - Obtain a report from People Operations analyst that contains the list of quota-carrying sales representatives for the specified quarter in excel or google sheet format including the fields shown [here](https://docs.google.com/document/d/10tdF7fHUdq2zehwIQQdUwumi9DXNT03o1PKCIkQYSww/edit). 

          - Post receipt of quota-carrying sales representatives report, identify the list of the certifiers by considering the below points; <br>
               1. Add a [column](https://docs.google.com/document/d/1LijtOGufWtW9X_hgHJvjNkHTTCkO3TSnPPQr9gNy3JA/edit) titled 'Justification for inclusion or exclusion'.<br>
               2. Add a [column](https://docs.google.com/document/d/1LijtOGufWtW9X_hgHJvjNkHTTCkO3TSnPPQr9gNy3JA/edit) titled 'included yes/ no'.<br>
               3. Update column specified in point (2) above as 'No' based on the below exclusions from the 'Job Title' column.<br>
                    - Inactive team members as on the date of Sales representation letter roll out.<br>
                    - Hired after the quarter-end if any included in the report.<br>
                    - Team members with below titles:<br>
                         -  Engineers<br>
                         -  Solutions Architect<br>
                         -  Technical Account Manager<br>
                         -  Any team member having 'marketing' in their title (specifically Senior Director - Corporate Marketing and Director, Marketing - Operations and their direct reports<br>
                         -  Community relations<br>
                         -  Director, Sales and Customer Enablement<br>
               4. Update column specified in point (2) 'included' Yes/ No with yes for the remaining list of team members.<br>
               5. Filter on 'Yes' in the column specified in point (2) 'included' Yes/ No and copy the data in a separate tab with name 'Qx FYxx - List of Certifiers' in the same report.<br>
               6. Create another tab and copy the list of certifiers from the previous quarter.<br>
               7. Reconcile the current list of certifiers with the previous quarter list of certifiers by using [vlookup](https://docs.google.com/document/d/1wl9EQeHmSnKQIJdGI7eZ4CXYBObBdg9VpuNA180vvqM/edit) in both the list and create a [reconciliation statement](https://docs.google.com/document/d/1sz1BEfC60KE3l1Dpcc3fEf4NlUx0i-nHxrPyMfpr3_w/edit).


     2. On the second day of the subsequent month after the quarter end -<br>
          - Finalize the content for the Sales Representation letter in a [google doc](https://docs.google.com/document/d/1E1Ni48QMkwBkemjg8-TU4UNDNmYv1KiYvT6t9aAxnKc/edit). 

          - Create the Sales Representation letter to be rolled out in a [google form](https://docs.google.com/forms/d/1qc5oamIzWv4DZdAa0YRwlnMqahyrZgjdXESOvRXkXKU/edit)<br>
               1.    Fill the content finalized under the ‘Questions’ tab.<br>
               2.    Under the Response tab, select ‘Get email notifications for new responses’ and also select the [response destination](https://docs.google.com/document/d/18iKnYWrbxrzlx45N6oebbOG8Gy15whTT9H6iZBTrqN8/edit) wherein the response spreadsheet should be saved.<br>
               3.    Go to settings, under the ‘[General](https://docs.google.com/document/d/18OiOLhE3mj3RuxoHIzBTxUzfvTG7r1dHfo1CrP6ZIao/edit)’ tab select the checkboxes shown here.<br>
               4.    Go to settings, under the ‘Presentation’ tab select the checkbox show progress bar, and fill in the [confirmation message](https://docs.google.com/document/d/117NeWRh3qVTwcrZnyrx5QEzug4HXFB1NKw41KB7b8TQ/edit#heading=h.9h992og1z66b) as shown [here](https://docs.google.com/document/d/1h1W2REaCUSp0FMQboBq-Hamv3OIEvw_Kg5mwgV5b6Dg/edit).
          - Create an [email message](https://docs.google.com/document/d/117NeWRh3qVTwcrZnyrx5QEzug4HXFB1NKw41KB7b8TQ/edit) content to send along with the google form. Email message should include the timeline for submission (Eighth day of the subsequent month after the quarter end) and a note describing the points to be noted while filling the form.

     3. On the third day of the subsequent month after the quarter end - Get sign-off on the list of certifiers and google form Principal Accounting Officer.

     4. On the fourth day of the subsequent month after the quarter end -  [Send](https://docs.google.com/document/d/1Y2rmoSIyjHw7Lt4Lnks63QA5eCuA37wZWkj_llfvFWk/edit) the Sales Representation google form to the list of certifiers. Also, add Principal Accounting Officer, Senior Internal Audit Manager and Senior Internal Auditor in the sender ‘To’ list while rolling out the google form.

     5. As and when response receipt notification is received on email, print and save each form in a pdf format in the folder (create a folder titled 'Sales Rep Responses for Qx 20XX').

     6. Responses received will get saved in the ‘Form responses’ sheet.<br>
          - Copy the current quarter ‘List of certifiers’ from the quota-carrying sales team members report in a separate tab in the response google sheet.<br>
          - Add additional columns shown [here](https://docs.google.com/document/d/1CevZ0NsY6RrwOXk69nstrqs3EOveen6_sGiJl7M1knI/edit). Under the response summary column, get the responses from the ‘Form Responses’ tab using vlookup. 


     7. Sending reminder emails:<br>
          - Send first [email](https://docs.google.com/document/d/117NeWRh3qVTwcrZnyrx5QEzug4HXFB1NKw41KB7b8TQ/edit#heading=h.49f7ics8iat9) to the pending certifiers for the deadline reminder via google form.<br>
          - Send two [followup emails](https://docs.google.com/document/d/117NeWRh3qVTwcrZnyrx5QEzug4HXFB1NKw41KB7b8TQ/edit#heading=h.l9nvm93ptmmc) to the pending certifiers post the deadline to sales representatives via google form.<br>
          - In case the responses are still pending after two reminders, send [email](https://docs.google.com/document/d/117NeWRh3qVTwcrZnyrx5QEzug4HXFB1NKw41KB7b8TQ/edit#heading=h.cvs53b52aoit) to respective Reporting managers of pending certifiers stating the pending responses from their direct reports marking cc to VP, Field Operations, Principal Accounting Officer, Senior Internal Audit Manager and Senior Internal Auditor.<br>
          - Send [email](https://docs.google.com/document/d/117NeWRh3qVTwcrZnyrx5QEzug4HXFB1NKw41KB7b8TQ/edit#heading=h.tggxw8r3mrjx) to VP, Field Operations and marking cc to respective Reporting managers, Principal Accounting Officer, Senior Internal Audit Manager and Senior Internal Auditor.<br>


     8. Collate the summary of the responses and prepare the [report](https://docs.google.com/presentation/d/1xdFPsEbslSEu2scyPVlQLHAE2knldpNFLDzpJ86Ku-g/edit#slide=id.g7daf7447b2_0_73) on the sales representation letter results.<br>
          -   Create an issue for each exception cases and tag respective people for further actions and follow-ups.<br>
          -   Close the issue post receipt of justification/resolution.<br>
          -   Discuss with Principal Accounting Officer on the pending status and issues/exception cases noted in the responses provided by sales representatives.


     9. Create an [issue](https://gitlab.com/gitlab-com/internal-audit/internal-audit/-/issues/312) and link the Report on Sales Representation letter results, and get the signoff on the report from the Principal Accounting Officer and Chief Financial Officer.
